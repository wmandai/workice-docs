User Guide
==========

This documentation will help you learn all about creating and creating projects, sending invoices, receiving payments, creating tasks, converting estimates to invoices, recurring invoices, entering credits, support system and much more.

.. _installation:

.. toctree::
   :maxdepth: 1
   :caption: Installation

   install
   configure
   update
   changelogs
   api
   client_portal

.. _basic-features:

.. toctree::
   :maxdepth: 1
   :caption: Basic Features

   introduction
   deals
   leads
   clients
   invoices
   payments
   recurring_invoices
   credits
   projects
   subscriptions
   knowledgebase
   estimates
   contracts
   tasks
   expenses
   calendar
   tickets
   reports
   settings

.. _advanced-settings:

.. toctree::
   :maxdepth: 1
   :caption: Advanced Settings

   system_settings
   lead_settings
   deal_settings
   invoice_settings
   payment_settings
   estimate_settings
   email_settings
   reminders
   email_templates
   translations
